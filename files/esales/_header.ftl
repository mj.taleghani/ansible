<header class="top-bar">
    <div class="top-bar-top show-for-large">
        <div class="row expanded collapse">
            <div class="column shrink">
                <h1 class="page-logo">
                    <a href="/">
                        <svg>
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/img/sprite.svg#hamrahLogoColored">
                        </svg>
                    </a>
                </h1>
            </div>
            <div class="column show-for-large">
                <ul class="menu vertical large-horizontal">
                    <li>
                        <h2>
                            هیچکس تنها نیست...
                        </h2>
                    </li>
                    <li>
                        <a class="is-active" href="https://mci.ir">
                            مشترکین حقیقی
                        </a>
                    </li>
                    <li>
                        <a href="https://www.mci.ir/web/business/home" target="_blank">
                            مشترکین سازمانی
                        </a>
                    </li>
                </ul>
            </div>
            <div class="column shrink show-for-large">
                <a href="https://my.mci.ir/" class="mymci-link" target="_blank">
                    <img src="/assets/img/my-mci-top-bar-logo.svg" alt="همراه من"/>
                </a>
            </div>
            <div class="column shrink">
                <a href="https://join.mci.ir/" class="tarabord" target="_blank">
                    <span>ترابرد به همراه اول</span>
                </a>
            </div>
        </div>
    </div>
    <div class="top-bar-bottom">
        <div class="row expanded collapse">
            <div class="column">
                <div class="side-menu-container" id="mainPageMenu" data-toggler=".is-open">
                    <button type="button" class="hamburger hide-for-large" data-toggle="mainPageMenu"></button>
                    <nav class="header-menu">
                        <button class="close-side-menu" type="button" data-toggle="mainPageMenu"></button>
                        <div class="header-menu-inner">
                            <h6 class="show-for-sr">منوی اصلی</h6>
                            <div class="side-menu-top hide-for-large">
                                <div class="media-object align-middle">
                                    <div class="media-object-section">
                                        <div class="page-logo side-menu-logo">
                                            <a href="/">
                                                <svg>
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/img/sprite.svg#hamrahLogoColored">
                                                </svg>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="media-object-section">
                                        <div class="side-menu-slogan">
                                                هیچکس تنها نیست...
                                        </div>
                                    </div>
                                </div>
                                <ul class="menu horizontal">
                                    <li>
                                        <a class="is-active" href="https://mci.ir" style="padding-left: 10px; padding-right: 10px;">
                                            مشترکین حقیقی
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.mci.ir/web/business/home" target="_blank" style="padding-left: 10px; padding-right: 10px;">
                                            مشترکین سازمانی
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <ul class="menu vertical large-horizontal">
                                <li class="has-submenu">
                                    <a class="no-link-js">
                                        سیم کارت
                                        <i class="zmdi zmdi-hc-fw zmdi-chevron-down float-left brand-blue-c hide-for-large"></i>
                                        <i class="zmdi zmdi-hc-fw zmdi-chevron-up float-left brand-blue-c hide-for-large" style="display: none;"></i>
                                        <i class="zmdi zmdi-chevron-down show-for-large"></i>
                                    </a>
                                    <div class="menu-pane">
                                        <div class="row expanded">
                                            <div class="column small-12 medium-8 large-6">
                                                <div class="row">
                                                    <div class="column small-12 large-3">
                                                        <h5 class="submenu-header">
                                                            <a href="/golchin/number/11">
                                                                سیم&zwnj;کارت دائمی
                                                            </a>
                                                        </h5>
                                                        <ul class="menu vertical">
                                                            <li>
                                                                <a href="/golchin/number/11">
                                                                    زمرد
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="/golchin/number/13">
                                                                    عادی
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="column small-12 large-3">
                                                        <h5 class="submenu-header">
                                                            <a href="/golchin/number/14">
                                                                سیم&zwnj;کارت اعتباری
                                                            </a>
                                                        </h5>
                                                        <ul class="menu vertical">
                                                            <li>
                                                                <a href="/golchin/number/14">
                                                                    عادی
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="column small-12 large-3">
                                                        <h5 class="submenu-header">
                                                            <a href="/golchin/number/23">
                                                                سیم&zwnj;کارت ویژه
                                                            </a>
                                                        </h5>
                                                        <ul class="menu vertical">
                                                            <li>
                                                                <a href="/golchin/number/23">
                                                                    تاریخ تولد
                                                                </a>
                                                            </li>
                                                            <#--<li>-->
                                                                <#--<a href="/golchin/number/28">-->
                                                                    <#--کمپین استانی-->
                                                                <#--</a>-->
                                                            <#--</li>-->
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <#if mainMenu??>
                                <#list mainMenu as menu>
                                    <#if (menu.submenus)??>
                                        <#assign hasSubmenu = true>
                                    <#else>
                                        <#assign hasSubmenu = false>
                                    </#if>
                                    <#if (menu.menuItemTitle != "سیم‌کارت") && (menu.menuItemTitle != "گلچین")>
                                        <li class="${hasSubmenu?then('has-submenu', '')}">
                                            <#if (menu.submenus)??>
                                                <a class="no-link-js">
                                                    ${menu.menuItemTitle}
                                                    <i class="zmdi zmdi-hc-fw zmdi-chevron-down float-left brand-blue-c hide-for-large"></i>
                                                    <i class="zmdi zmdi-hc-fw zmdi-chevron-up float-left brand-blue-c hide-for-large" style="display: none;"></i>
                                                    <i class="zmdi zmdi-chevron-down show-for-large"></i>
                                                </a>
                                            <#else>
                                                <a href="${menu.menuItemURL}">
                                                    ${menu.menuItemTitle}
                                                    <i class="zmdi zmdi-hc-fw zmdi-chevron-left float-left brand-blue-c hide-for-large"></i>
                                                </a>
                                            </#if>
                                            <#if (menu.submenus)??>
                                                <#list menu.submenus>
                                                    <div class="menu-pane">
                                                        <div class="row expanded">
                                                            <div class="column small-12 medium-8 large-6">
                                                                <div class="row">
                                                                    <#items as submenu>
                                                                        <div class="column small-12 large-3">
                                                                            <h5 class="submenu-header">
                                                                                <a href="${submenu.submenuURL}">
                                                                                    ${submenu.submenuTitle}
                                                                                </a>
                                                                            </h5>
                                                                            <#list submenu.submenuItems>
                                                                                <ul class="menu vertical">
                                                                                    <#items as submenuItem>
                                                                                        <li>
                                                                                            <a href="${submenuItem.submenuItemURL}">
                                                                                                ${submenuItem.submenuItemTitle}
                                                                                            </a>
                                                                                        </li>
                                                                                    </#items>
                                                                                </ul>
                                                                            </#list>
                                                                        </div>
                                                                    </#items>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </#list>
                                            </#if>
                                        </li>
                                    </#if>
                                </#list>
                                </#if>
                                <li>
                                    <a href="/charge">
                                        شارژ
                                        <i class="zmdi zmdi-hc-fw zmdi-chevron-left float-left brand-blue-c hide-for-large"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="/package">
                                        بسته‌ها
                                        <i class="zmdi zmdi-hc-fw zmdi-chevron-left float-left brand-blue-c hide-for-large"></i>
                                    </a>
                                </li>
                            </ul>
                            <div class="side-menu-bottom hide-for-large">
                                <!-- Social Links -->
                                <#include "_footerSocials.ftl"/>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="page-logo-small hide-for-large">
                    <svg>
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/img/sprite.svg#hamrahLogoWhite">
                    </svg>
                </div>
            </div>
            <div class="column shrink">
                <div class="row collapse">
                    <div class="column shrink show-for-medium hide-for-large">
                        <a href="https://my.mci.ir/" class="mymci-link mymci-small" target="_blank">
                            <img src="/assets/img/my-mci-top-bar-logo.svg" alt="همراه من"/>
                        </a>
                    </div>
                    <div class="column shrink">
                        <!-- Search -->
                        <#include "_headerSearch.ftl"/>
                    </div>
                    <div class="column shrink">
                        <!-- Login Control -->
                        <#include "_loginControl.ftl"/>
                    </div>
                    <div class="column shrink">
                        <!-- Shopping Cart -->
                        <#include "_shoppingCart.ftl"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
