<#import "../layout/common.ftl" as com>
<#import "../lib/radio.ftl" as radio>

<@com.page title="لیست محصولات" pageType="market-page white-bg"; section>
    <#if section="content">
        <div class="row">
            <div class="column sall-12">
                <nav aria-label="شما اینجا هستید:" role="navigation">
                    <ul class="breadcrumbs">
                        <li><a href="\">فروشگاه همراه اول</a></li>
                        <li>
                            <span class="show-for-sr">فعلی: </span> سیم کارت
                        </li>
                    </ul>
                </nav>

                <!-- TODO: from homam: this should be replaced with banner Items from admin  -->
                <div class="row">
                    <div class="columns small-12 text-center">
                        <a href="/golchin/number/23" class="ads-item" target="_blank">
                            <img src="/assets/img/banners/banner.jpg" width="" height="" alt="ad-banner">
                        </a>
                    </div>
                </div>

                <div class="row small-up-1 medium-up-3 golchin-tab-titles">
                    <#list simData as cat>
                        <div class="column">
                            <div class="golchin-tab-title" data-href="#${cat.categoryTypeEn}">
                                <div class="tab-title-container">
                                    <svg>
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/img/sprite.svg#${cat.categoryTypeEn}GolchinSim">
                                    </svg>
                                    <div class="text">${cat.categoryTypeFa}</div>
                                </div>
                                <ul class="tab-title-menu">
                                    <#list cat.categorySim as subCat>
                                        <#if (subCat.show == true) && (subCat.selected)?? && (subCat.selected == true)>
                                             <li>
                                                 <a href="javascript:" class="${subCat.selected?then('is-active', '')} ${subCat.enabled?then('', 'disabled')}" data-purpose="${subCat.purpose}" data-parent="${subCat.id}">
                                                    ${subCat.simColorFa}
                                                </a>
                                            </li>
                                        <#elseif (subCat.show)?? && (subCat.show == true)>
                                            <li>
                                                <a href="${(subCat.linkURL??)?then(subCat.linkURL, 'javascript:;')}" class="${subCat.selected?then('is-active', '')} ${subCat.enabled?then('', 'disabled')}" data-parent="${subCat.id}">
                                                    ${subCat.simColorFa}
                                                </a>
                                            </li>
                                        </#if>
                                    </#list>
                                </ul>
                            </div>
                        </div>
                    </#list>
                </div>

                <#list simData as cat>
                    <#list cat.categorySim as subCat>
                        <#if (subCat.selected)?? && (subCat.selected == true)>
                            <#if message??>
                                <div class="callout alert text-center system-message">
                                    ${message}
                                </div>
                            </#if>
                            <div id="simSelect" class="sim-category ${subCat.simColorEn}">
                                <div class="row align-middle sim-category-title">
                                    <div class="column small-12 medium-4">
                                        <div class="media-object">
                                            <div class="media-object-section">
                                                <img src="${subCat.imageURL}" alt="${cat.categoryTypeFa} ${subCat.simColorFa}" />
                                            </div>
                                            <div class="media-object-section main-section">
                                                <h4>
                                                    <div>${cat.categoryTypeFa}</div>
                                                    <div>${subCat.simColorFa}</div>
                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column small-12 medium-2">
                                        <div class="row sim-desc">
                                            <div class="column medium-12">
                                                <div class="svg-container">
                                                    <svg>
                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/img/sprite.svg#earthWireframe">
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="column medium-12">
                                                <h5>${subCat.options[0].desc}</h5>
                                                <p>${subCat.options[0].title}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column small-12 medium-2">
                                        <div class="row sim-desc">
                                            <div class="column medium-12">
                                                <div class="svg-container">
                                                    <svg>
                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/img/sprite.svg#checkCircle">
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="column medium-12">
                                                <h5>${subCat.options[1].desc}</h5>
                                                <p>${subCat.options[1].title}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column small-12 medium-2">
                                        <div class="row sim-desc">
                                            <div class="column medium-12">
                                                <div class="svg-container">
                                                    <svg>
                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/img/sprite.svg#rialCircle">
                                                    </svg>
                                                </div>
                                            </div>
                                            <div class="column medium-12">
                                                <h5>${subCat.options[2].desc}</h5>
                                                <p>${subCat.options[2].title}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <#--<div class="column small-12 medium-2" style="display: none;">-->
                                        <#--<button type="button" class="button button-brand shadowed show-prefix" data-purpose="${subCat.purpose}">-->
                                            <#--مشاهده شماره‌ها-->
                                            <#--<i class="zmdi zmdi-hc-fw zmdi-long-arrow-down"></i>-->
                                        <#--</button>-->
                                    <#--</div>-->
                                </div>

                                <!--  number selection  -->
                                <div class="callout secondary batch-selection-container">
                                    <div class="row">
                                        <#if (groupItems)??>
                                            <div class="column small-12 medium-6 large-3">
                                                <div class="input-container">
                                                    <div class="row align-center-middle">
                                                        <div class="column small-12 medium-4">
                                                            <label for="numberPrefix">نوع:</label>
                                                        </div>
                                                        <div class="column">
                                                            <div class="input-inner-container">
                                                                <select id="simType" name="simType" aria-describedby="نوع سیم کارت" data-parent="${groupItems[0].parent}">
                                                                    <#if groupItems??>
                                                                        <#list groupItems as item>
                                                                            <#if item.purpose == purpose>
                                                                                <option value="${item.linkURL}" selected>${(item.simColorFa == "تاریخ تولد دائمی")?then('دائمی', 'اعتباری')}</option>
                                                                            <#else>
                                                                                <option value="${item.linkURL}">${(item.simColorFa == "تاریخ تولد دائمی")?then('دائمی', 'اعتباری')}</option>
                                                                            </#if>
                                                                        </#list>
                                                                    </#if>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </#if>
                                        <div class="column small-12 medium-6 large-3">
                                            <div class="input-container">
                                                <div class="row align-center-middle">
                                                    <div class="column small-12 medium-4">
                                                        <label for="numberPrefix">پیش کد:</label>
                                                    </div>
                                                    <div class="column">
                                                        <div class="input-inner-container">
                                                            <select id="numberPrefix" data-purpose="${subCat.purpose}" name="numberPrefix" aria-describedby="پیش کد">
                                                                <option disabled selected>انتخاب پیش کد</option>
                                                                <#if prefixes??>
                                                                    <#list prefixes as prefix>
                                                                        <option value="${prefix}">${prefix}</option>
                                                                    </#list>
                                                                </#if>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column small-12 medium-6 large-3">
                                            <div class="input-container">
                                                <div class="row align-center-middle">
                                                    <div class="column small-12 medium-4">
                                                        <label for="specialCampaign">طرح ویژه:</label>
                                                    </div>
                                                    <div class="column">
                                                        <div class="input-inner-container">
                                                            <i class="zmdi zmdi-hc-lg zmdi-globe-alt icon-right" style="color: #34CA00;"></i>
                                                            <select id="specialCampaign" name="specialCampaign" aria-describedby="طرح ویژه" disabled>
                                                                <option disabled selected>${subCat.options[0].desc}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <#if !(groupItems)??>
                                            <div class="column show-for-medium medium-6 large-3"></div>
                                        </#if>
                                        <div class="column small-12 medium-6 large-3">
                                            <div class="input-container">
                                                <div class="row align-center-middle">
                                                    <div class="column small-12 medium-4">
                                                        <label for="numberMask">سرشماره:</label>
                                                    </div>
                                                    <div class="column">
                                                        <div class="input-inner-container">
                                                            <i class="zmdi zmdi-hc-lg zmdi-card-sim icon-right" style="color: #34CA00;"></i>
                                                            <select id="numberMask" name="numberMask" aria-describedby="سرشماره" disabled>
                                                                <option disabled="disabled" selected="selected">انتخاب سرشماره</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--  Captcha  -->
                                <div class="row align-middle align-center golchin-captcha-container" style="display: none;">
                                    <div class="column shrink">
                                        <form action="" name="captchaForm" id="captchaForm" data-abide novalidate>
                                            <div class="input-container">
                                                <div class="row align-center-middle">
                                                    <div class="column shrink">
                                                        <label for="captcha">کد امنیتی:</label>
                                                    </div>
                                                    <div class="column">
                                                        <span class="form-error" data-form-error-for="captcha">
                                                            کد امنیتی را وارد کنید
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="row align-top align-justify">
                                                    <div class="column small-12 medium-4">
                                                        <div class="input-inner-container">
                                                            <input type="text" id="captcha" name="enteredValue" aria-describedby="کد امنیتی" required autocomplete="off">
                                                            <button type="button" class="icon-left" id="refreshCaptcha"><i class="zmdi zmdi-refresh-alt"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="column small-12 medium-4">
                                                        <div class="input-inner-container text-center medium-text-left">
                                                            <img style="width: auto; height: auto;" src="" id="captchaImg">
                                                            <input type="hidden" value="" id="captchaId" name="captchaId">
                                                        </div>
                                                    </div>
                                                    <div class="column small-12 medium-4">
                                                        <button type="submit" class="button expanded button-brand get-numbers shadowed">
                                                            مشاهده شماره‌ها
                                                            <i class="zmdi zmdi-hc-fw zmdi-long-arrow-down"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <!--  numbers container  -->
                                <div class="batch-numbers-container" style="display: none;">
                                    <div class="row">
                                        <div class="column small-12 medium-8 large-9">
                                            <div class="card no-border">
                                                <div class="card-section">
                                                    <h5>شماره‌های قابل انتخاب</h5>
                                                </div>
                                                <div class="card-section">
                                                    <div class="selectable-numbers-container">
                                                        <div class="selectable-numbers ajax-disabled">
                                                        </div>
                                                    </div>
                                                    <nav aria-label="Pagination">
                                                        <div class="golchin-pagination-container">
                                                            <div class="row collapse align-center">
                                                                <div class="column shrink">
                                                                    <ul class="pagination golchin-pagination-previous">
                                                                        <li class="pagination-previous"><a id="goToPrev" aria-label="Previous page"><span class="show-for-sr">صفحه قبل</span></a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="column shrink">
                                                                    <ul class="pagination golchin-pagination"></ul>
                                                                </div>
                                                                <div class="column shrink">
                                                                    <ul class="pagination golchin-pagination-next">
                                                                        <li class="pagination-next"><a id="goToNext" aria-label="Next page"><span class="show-for-sr">صفحه بعد</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </nav>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="column small-12 medium-4 large-3">
                                            <div class="card">
                                                <div class="card-divider">
                                                    <h5>شماره‌های رزرو شده</h5>
                                                </div>
                                                <div class="card-section">
                                                    <div class="selected-numbers-container">
                                                        <div class="row small-up-2 golchin-selected">
                                                            <#if golchinPocket??>
                                                                <#list golchinPocket as number>
                                                                    <div class="column">
                                                                        <div class="golchin-selected-button">
                                                                            <button type="button" id="selected_${number.msisdn}" data-purpose="${number.purpose}">
                                                                                <svg class="checked">
                                                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/img/sprite.svg#closeTimes"></use>
                                                                                </svg>
                                                                            </button>
                                                                            ${number.msisdn}
                                                                        </div>
                                                                    </div>
                                                                </#list>
                                                            </#if>
                                                        </div>
                                                    </div>
                                                    <!-- Stucture for radio button and checkbox face-off -->
                                                    <label for="readPolicy">
                                                        <!-- Stucture for radio button and checkbox face-off -->
                                                        <span class="checkbox brand-orange">
                                                            <input type="checkbox" id="readPolicy" required/>
                                                            <i class="zmdi zmdi-check-square"></i>
                                                            <i class="zmdi zmdi-square-o"></i>
                                                        </span>
                                                        <#--<a href="/conditions" class="inline-button" target="_blank">-->
                                                            <#--<span class="brand-orange-c">شرایط و قوانین</span>-->
                                                        <#--</a>-->
                                                        <button type="button" class="inline-button terms-conditions ">
                                                            <span class="brand-orange-c">شرایط و قوانین</span>
                                                        </button>
                                                        خرید محصولات همراه اول را مطالعه کرده و با کلیه موارد آن موافقم.
                                                    </label>
                                                    <button type="button" class="button expanded success add-sim-to-cart">
                                                        <i class="zmdi zmdi-hc-lg zmdi-shopping-cart-plus"></i>
                                                        افزودن به سبد خرید
                                                    </button>
                                                </div>
                                                <div class="hide">
                                                    <#if terms??>
                                                        ${terms}
                                                    <#else>
                                                        <p>در حال حاضر قوانین و مقررات در سایت بارگزاری نشده لطفا مجدد تلاش کنید.</p>
                                                    </#if>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </#if>
                    </#list>
                </#list>
            </div>
        </div>
    </#if>
    <#if section="scriptsEnd">
        <script src="/assets/common-js/slick.js"></script>
        <script src="/assets/common-js/golchin.js"></script>
    </#if>
</@com.page>

