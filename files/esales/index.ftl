<#import "layout/common.ftl" as com>
<@com.page title="صفحه اصلی" pageType="home-page"; section>
    <#if section="content">
        <div class="jumbotron-container">
            <div class="orbit" role="region" aria-label="تبلیغات بزرگ صفحه" data-orbit data-orbit data-options="animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out;">
                <div class="orbit-wrapper">
                    <ul class="orbit-container">
                        <li class="orbit-slide">
                            <figure class="orbit-figure">
                                <a href="/golchin/number/23" target="_blank">
                                    <img class="orbit-image" src="/assets/img/banners/slider-tavalod.jpg" alt="">
                                </a>
                            </figure>
                        </li>
                        <li class="orbit-slide">
                            <figure
                                    class="orbit-figure">
                                <a href="/charge" target="_blank">
                                    <img class="orbit-image" src="/assets/img/banners/home-slide-2.jpg" alt="">
                                </a>
                            </figure>
                        </li>
                        <li class="orbit-slide">
                            <figure class="orbit-figure">
                                <a href="/catalog/category/70" target="_blank">
                                    <img class="orbit-image" src="/assets/img/banners/home-slide-3.jpg" alt="">
                                </a>
                            </figure>
                        </li>
                        <li class="orbit-slide">
                            <figure class="orbit-figure">
                                <a href="/package" target="_blank">
                                    <img class="orbit-image" src="/assets/img/banners/home-slide-4.jpg" alt="">
                                </a>
                            </figure>
                        </li>
                        <li class="orbit-slide">
                            <figure class="orbit-figure">
                                <a href="/golchin/number/11" target="_blank">
                                    <img class="orbit-image" src="/assets/img/banners/home-slide-5.jpg" alt="">
                                </a>
                            </figure>
                        </li>
                    </ul>
                    <div class="orbit-indicator">
                        <div class="orbit-indicator-bar"></div>
                    </div>
                </div>
                <nav class="orbit-bullets">
                    <button class="orbit-bullet is-active" data-slide="0"><span class="show-for-sr">اسلاید اول</span><span class="show-for-sr">اسلاید فعلی</span></button>
                    <button class="orbit-bullet" data-slide="1"><span class="show-for-sr">اسلاید دوم</span></button>
                    <button class="orbit-bullet" data-slide="2"><span class="show-for-sr">اسلاید سوم</span></button>
                    <button class="orbit-bullet" data-slide="3"><span class="show-for-sr">اسلاید چهارم</span></button>
                    <button class="orbit-bullet" data-slide="4"><span class="show-for-sr">اسلاید پنجم</span></button>
                </nav>
            </div>
        </div>

        <#include "partials/_homeServicePromotion.ftl"/>

        <div class="row">
            <div class="columns small-12 text-center">
                <a href="/golchin/number/23" class="ads-item" target="_blank">
                    <img src="/assets/img/banners/banner.jpg" width="" height="" alt="ad-banner">
                </a>
            </div>
        </div>

        <div class="row">
            <div class="column small-12">
                <div class="callout outer-container">
                    <#include "market/_indexSuggestedProducts.ftl"/>
                </div>
            </div>
        </div>

        <#include "market/_recnetProducts.ftl"/>

        <#include "market/_homeFixedBg.ftl"/>

        <#include "market/_bestSeller.ftl"/>
    </#if>
    <#if section="scripts">
        <script src="/assets/common-js/slick.js"></script>
    </#if>
</@com.page>
